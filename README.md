Visual-Experiments.com code playground :

    ArToolKitPlusDemo: ArToolKitPlus integration with Ogre3D.

    Ogre::Cuda: Cuda 3.0 integration with Ogre3D: allow to register/map DirectX or OpenGL texture with Cuda graphic interop.

    Ogre::OpenCL: OpenCl integration with Ogre3D: designed with the same goal as Ogre::Cuda but for OpenCL.

    Ogre::GPGPU: GPGPU helper for Ogre3D: add an abstraction over texture/pixel shader rename in result/operation.

    Ogre::Canvas: 2D API for Ogre3D using Skia: implemented using the same API as Html5 Canvas tag with JS bindings.

    GPUSurf: GPU-Surf implemented using Ogre::Cuda, Ogre::GPGPU and Ogre::Canvas for visual debuging: this is a partial implementation (descriptor not implemented, just detector). 